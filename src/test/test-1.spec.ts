import { ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from 'src/app/app.component';
import { CalcularSerieComponent } from 'src/app/Components/calcular-serie/calcular-serie.component';
import { FormularioComponent } from 'src/app/Components/formulario/formulario.component';
import { MostrarSerieComponent } from 'src/app/Components/mostrar-serie/mostrar-serie.component';
import { MaterialModule } from 'src/app/Modules/material.module';
import { SharedModule } from 'src/app/Modules/shared/shared.module';


describe('AppComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                FormularioComponent,
                CalcularSerieComponent,
                MostrarSerieComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                MaterialModule,
                SharedModule
            ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]

        }).compileComponents();
    });

    // it('Se ingresa valor cero (0), debe mostrar: El valor ingresado debe de ser mayor a cero (0).', () => {

    //     /* Crear Formulario Component */
    //     const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
    //     const formularioComponent = fixtureFormularioComponent.componentInstance;

    //     const number = formularioComponent.serieForm.get('number');
    //     number?.setValue(1);

    //     fixtureFormularioComponent.detectChanges();

    //     const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
    //     btnCalcular.nativeElement.click();

    //     /* Crear Mostrar Serie Component */
    //     const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
    //     fixtureMostrarSerieComponent.detectChanges();

    //     const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

    //     expect(wrapperError.querySelector('.wrapper-error')?.textContent).toContain('Error: No se puede calcular el número primo (-1)');
    // });

    it('Se ingresa valor uno (1), debe mostrar: Error: No se puede calcular el número primo (-1).', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(1);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.wrapper-error')?.textContent).toContain('Error: No se puede calcular el número primo (-1)');
    });

    it('Se ingresa valor dos (2), debe mostrar: Error: No se puede calcular el número primo (0).', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(2);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.wrapper-error')?.textContent).toContain('Error: No se puede calcular el número primo (0)');
    });

    it('Se ingresa valor tres (3), debe mostrar el resultado: 1.71429', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(3);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('1.71429');
    });

    it('Se ingresa valor cuatro (4), debe mostrar el resultado: 3.85714', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(4);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('3.85714');
    });


    it('Se ingresa valor cinco (5), debe mostrar el resultado: 8.57143', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(5);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('8.57143');
    });

    it('Se ingresa valor seis (6), debe mostrar el resultado: 12', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(6);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('12');
    });


    it('Se ingresa valor siete (7), debe mostrar el resultado: 17.67857', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(7);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('17.67857');
    });

    it('Se ingresa valor ocho (8), debe mostrar el resultado: 18', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(8);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('18');
    });

    it('Se ingresa valor nueve (9), debe mostrar el resultado: 19.42857', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(9);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('19.42857');
    });

    it('Se ingresa valor diez (10), debe mostrar el resultado: 17.2437', () => {

        /* Crear Formulario Component */
        const fixtureFormularioComponent = TestBed.createComponent(FormularioComponent);
        const formularioComponent = fixtureFormularioComponent.componentInstance;

        const number = formularioComponent.serieForm.get('number');
        number?.setValue(10);

        fixtureFormularioComponent.detectChanges();

        const btnCalcular = fixtureFormularioComponent.debugElement.query(By.css('#btn-calcular'));
        btnCalcular.nativeElement.click();

        /* Crear Calcular Serire Component */
        const fixtureCalcularSerieComponent = TestBed.createComponent(CalcularSerieComponent);
        const calcularSerieComponent = fixtureCalcularSerieComponent.componentInstance;

        /* Crear Mostrar Serie Component */
        const fixtureMostrarSerieComponent = TestBed.createComponent(MostrarSerieComponent);
        fixtureMostrarSerieComponent.detectChanges();

        const wrapperError = fixtureMostrarSerieComponent.nativeElement as HTMLElement;

        expect(wrapperError.querySelector('.resultado')?.textContent).toContain('17.2437');
    });

});
