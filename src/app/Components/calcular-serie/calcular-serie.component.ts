import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SngularSerieService } from 'src/app/Services/serie/sngular-serie.service';

@Component({
  selector: 'app-calcular-serie',
  template: '',
})

export class CalcularSerieComponent implements OnDestroy {

  private subscription = new Subscription();

  constructor(
    private serieService: SngularSerieService
  ) {
    const serieNumber$ = serieService.number$.subscribe(number => {
      number && this.calculateSerie(number);
    });
    this.subscription.add(serieNumber$);
  }

  calculateSerie(number: number) {
    try {
      const prime = this.serieService.getPrimeNumber(number - 2);
      const triangular = this.serieService.getTriangularNumber(number - 2);
      const fibonacci = this.serieService.getFibonacciNumber(number - 1);
      this.serieService.valueSerie = {
        value: (2 * prime * 3 * triangular) / (7 * fibonacci),
        error: null
      };
    } catch (error) {
      this.serieService.valueSerie = { value: null, error }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
