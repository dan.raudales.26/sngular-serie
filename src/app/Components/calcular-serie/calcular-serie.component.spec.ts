import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcularSerieComponent } from './calcular-serie.component';

describe('CalcularSerieComponent', () => {
  let component: CalcularSerieComponent;
  let fixture: ComponentFixture<CalcularSerieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcularSerieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcularSerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
