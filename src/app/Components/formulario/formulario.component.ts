import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SngularSerieService } from 'src/app/Services/serie/sngular-serie.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent {

  @ViewChild('ngSerieForm') ngSerieForm: any;

  serieForm = new FormGroup({
    number: new FormControl('', Validators.compose([Validators.required, Validators.min(1)]))
  });

  constructor(
    private serieService: SngularSerieService
  ) {
  }

  keyPressNumber(event: KeyboardEvent) {
    if (!/\d/.test(event.key)) return false
    return true;
  }

  calculateSerie() {
    this.serieService.number = this.serieForm.value.number;
    this.ngSerieForm.resetForm();
  }

}
