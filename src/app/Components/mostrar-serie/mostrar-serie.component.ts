import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SngularSerieService } from 'src/app/Services/serie/sngular-serie.service';

@Component({
  selector: 'app-mostrar-serie',
  templateUrl: './mostrar-serie.component.html',
  styleUrls: ['./mostrar-serie.component.scss']
})
export class MostrarSerieComponent implements OnDestroy {

  private subscription = new Subscription();
  number: number | null | undefined;
  result: number | undefined;
  error: any;

  constructor(
    serieService: SngularSerieService
  ) {
    const number$ = serieService.number$.subscribe(number => {
      this.number = number
    })
    this.subscription.add(number$);
    const valueSerie$ = serieService.valueSerie$.subscribe(serie => {
      if (serie) {
        this.error = serie.error;
        this.result = Number(serie.value?.toFixed(5));
      }
    });
    this.subscription.add(valueSerie$);

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
