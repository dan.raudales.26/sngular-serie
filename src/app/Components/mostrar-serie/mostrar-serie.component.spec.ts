import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarSerieComponent } from './mostrar-serie.component';

describe('MostrarSerieComponent', () => {
  let component: MostrarSerieComponent;
  let fixture: ComponentFixture<MostrarSerieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarSerieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarSerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
