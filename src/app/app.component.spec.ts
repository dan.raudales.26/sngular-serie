import { TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { CalcularSerieComponent } from './Components/calcular-serie/calcular-serie.component';
import { FormularioComponent } from './Components/formulario/formulario.component';
import { MostrarSerieComponent } from './Components/mostrar-serie/mostrar-serie.component';
import { MaterialModule } from './Modules/material.module';
import { SharedModule } from './Modules/shared/shared.module';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FormularioComponent,
        CalcularSerieComponent,
        MostrarSerieComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        SharedModule
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
