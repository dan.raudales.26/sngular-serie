import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TriangularNumberService {

  getTriangularNumber(number: number): number {
    return this.isOdd(number) ? this.calculateTringular(number - 1) + number : this.calculateTringular(number);
  }

  private calculateTringular(number: number): number {
    return (1 + number) * (number / 2);
  }

  private isOdd(number: number): number {
    return number % 2;
  }
}
