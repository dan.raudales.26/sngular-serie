import { TestBed } from '@angular/core/testing';

import { TriangularNumberService } from './triangular-number.service';

describe('TriangularNumberService', () => {
  let service: TriangularNumberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TriangularNumberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
