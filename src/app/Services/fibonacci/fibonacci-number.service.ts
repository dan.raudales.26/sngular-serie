import { Injectable } from '@angular/core';;

@Injectable({
  providedIn: 'root'
})
export class FibonacciNumberService {

  getFibonacciNumber(number: number): number {
    return this.calculateFibonacciNumber(number);
  }


  calculateFibonacciNumber(number: number, prevNumber = 0, nextNumber = 1): number {
    if (number < 1) return prevNumber;
    if (number < 2) return nextNumber;
    let p = number - 1;
    [prevNumber, nextNumber] = [nextNumber, prevNumber + nextNumber];
    return this.calculateFibonacciNumber(p, prevNumber, nextNumber);
  }

}
