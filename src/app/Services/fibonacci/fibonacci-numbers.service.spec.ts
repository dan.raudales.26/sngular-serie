import { TestBed } from '@angular/core/testing';

import { FibonacciNumberService } from './fibonacci-number.service';

describe('FibonacciNumberService', () => {
  let service: FibonacciNumberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FibonacciNumberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
