import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PrimeNumberService {

  private primeNumbers: Array<number> = [];

  getPrimeNumbers(number: number): number {
    if (number > this.primeNumbers.length) {
      this.calculatePrimeNumbers(number);
    }
    return this.primeNumbers.slice(0, number).pop() as number;
  }

  private calculatePrimeNumbers(number: number) {
    let size = this.primeNumbers.length,
      count = size ? this.primeNumbers[size - 1] + 1 : 2;
    while (this.primeNumbers.length < number) {
      this.isPrime(count) && this.primeNumbers.push(count);
      count++;
    }
  }

  private isPrime(number: number): boolean {
    if (number <= 1) return false;
    for (let i = 2; i <= Math.sqrt(number); i++) {
      if (number % i === 0) return false;
    }
    return true;
  }

}
