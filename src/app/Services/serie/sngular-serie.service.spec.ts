import { TestBed } from '@angular/core/testing';

import { SngularSerieService } from './sngular-serie.service';

describe('SngularSerieService', () => {
  let service: SngularSerieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SngularSerieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
