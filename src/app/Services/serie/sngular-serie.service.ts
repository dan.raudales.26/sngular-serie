import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Serie } from 'src/app/Interfaces/serie.interface';
import { FibonacciNumberService } from '../fibonacci/fibonacci-number.service';
import { PrimeNumberService } from '../prime/prime-number.service';
import { TriangularNumberService } from '../triangular/triangular-number.service';


@Injectable({
  providedIn: 'root'
})
export class SngularSerieService {

  number$ = new BehaviorSubject<number | null>(null);
  valueSerie$ = new BehaviorSubject<Serie | null>(null);

  set number(number: number | null) {
    this.number$.next(number);
  }

  set valueSerie(serie: Serie) {
    this.valueSerie$.next(serie);
  }

  constructor(
    private fibonacciService: FibonacciNumberService,
    private primeService: PrimeNumberService,
    private triangularService: TriangularNumberService,
  ) { }

  getPrimeNumber(number: number): number {
    if (number < 1) throw new Error(`No se puede calcular el número primo (${number})`);
    return this.primeService.getPrimeNumbers(number);
  }

  getFibonacciNumber(number: number): number {
    if (number < 1) throw new Error(`No se puede calcular el número fibonacci (${number})`);
    return this.fibonacciService.getFibonacciNumber(number);
  }

  getTriangularNumber(number: number): number {
    if (number < 1) throw new Error(`No se puede calcular el número tringular (${number})`);
    return this.triangularService.getTriangularNumber(number);
  }

}
