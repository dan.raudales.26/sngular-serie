import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormularioComponent } from './Components/formulario/formulario.component';
import { CalcularSerieComponent } from './Components/calcular-serie/calcular-serie.component';
import { MostrarSerieComponent } from './Components/mostrar-serie/mostrar-serie.component';

/* Importar modulos */
import { MaterialModule } from './Modules/material.module';
import { SharedModule } from './Modules/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    CalcularSerieComponent,
    MostrarSerieComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
